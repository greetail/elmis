# README #

### What is this repository for? ###

* Learning SPA pages using knockoutjs, webapi, entityframework.

### How do I get set up? ###

* Install sql server on your machine
* run the application. Automated migrations are enabled, so on the first run a database with all the necessary tables will be created for you.

### Contribution guidelines ###

* List of features and bugs can be found on Trello https://trello.com/b/1IzZpGRW
* if updating the model, make sure to create the code first migrations. See http://kbiapps.com/entity-framework-code-first-migration-cheat-sheet/ for a reminder on code first migrations.
* There are currently no tests, but creating a test suite will be greatly appreciated :)
* You are a developer, don't break what is already working.  There is no code review here, just use branches, test your stuff before creating a pull request.
* Every commit to the master branch gets deployed to a "production" environment hosted at appharbor.

### Who do I talk to? ###

* Jose Delgado (jdelgado@greetail.co)