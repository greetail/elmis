namespace elmis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetOwnerIdAndDates : DbMigration
    {
        public override void Up()
        {
           
            AddColumn("dbo.MissionCommands", "OwnerId", c => c.String());
            AddColumn("dbo.MissionCommands", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.MissionCommands", "Updated", c => c.DateTime(nullable: false));
			AddColumn("dbo.Campaigns", "OwnerId", c => c.String());
			AddColumn("dbo.Campaigns", "Created", c => c.DateTime(nullable: false));
			AddColumn("dbo.Campaigns", "Updated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Missions", "OwnerId", c => c.String());
            AddColumn("dbo.Missions", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Missions", "Updated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Assets", "OwnerId", c => c.String());
            AddColumn("dbo.Assets", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Assets", "Updated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ribbons", "OwnerId", c => c.String());
            AddColumn("dbo.Ribbons", "Created", c => c.DateTime(nullable: false));
            AddColumn("dbo.Ribbons", "Updated", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ribbons", "Updated");
            DropColumn("dbo.Ribbons", "Created");
            DropColumn("dbo.Ribbons", "OwnerId");
            DropColumn("dbo.Assets", "Updated");
            DropColumn("dbo.Assets", "Created");
            DropColumn("dbo.Assets", "OwnerId");
            DropColumn("dbo.Missions", "Updated");
            DropColumn("dbo.Missions", "Created");
            DropColumn("dbo.Missions", "OwnerId");
            DropColumn("dbo.MissionCommands", "Campaign_Id");
            DropColumn("dbo.MissionCommands", "Updated");
            DropColumn("dbo.MissionCommands", "Created");
            DropColumn("dbo.MissionCommands", "OwnerId");
			DropColumn("dbo.Campaigns", "Updated");
			DropColumn("dbo.Campaigns", "Created");
			DropColumn("dbo.Campaigns", "OwnerId");
        }
    }
}
