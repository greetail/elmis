namespace elmis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Militarizemodel : DbMigration
    {
        public override void Up()
        {
			DropForeignKey("AspNetUsers", "FK_dbo.AspNetUsers_dbo.Lists_List_Id");
			DropForeignKey("AspNetUsers", "FK_dbo.AspNetUsers_dbo.Tickets_Ticket_Id");
			DropForeignKey("Attachments", "FK_dbo.Attachments_dbo.Tickets_Ticket_Id");
			DropForeignKey("Labels", "FK_dbo.Labels_dbo.Tickets_Ticket_Id");
			DropForeignKey("Tickets", "FK_dbo.Tickets_dbo.Lists_List_Id");
			DropIndex("AspNetUsers", "IX_TicketList_Id");
			DropIndex("AspNetUsers", "IX_Ticket_Id");
            DropIndex("Tickets", "IX_TicketList_Id");
			DropIndex("Attachments", "IX_Ticket_Id");
			DropIndex("Labels", "IX_Ticket_Id");
			RenameTable(name: "Attachments", newName: "Assets");
			RenameTable(name: "Labels", newName: "Ribbons");
            CreateTable(
                "MissionCommands",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Missions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        MissionCommand_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("MissionCommands", t => t.MissionCommand_Id)
                .Index(t => t.MissionCommand_Id);
            
            AddColumn("AspNetUsers", "Mission_Id", c => c.Int());
            AddColumn("Assets", "Mission_Id", c => c.Int());
            AddColumn("Ribbons", "Mission_Id", c => c.Int());
            CreateIndex("Assets", "Mission_Id");
            CreateIndex("Ribbons", "Mission_Id");
            CreateIndex("AspNetUsers", "Mission_Id");
            AddForeignKey("Assets", "Mission_Id", "Missions", "Id");
            AddForeignKey("Ribbons", "Mission_Id", "Missions", "Id");
            AddForeignKey("AspNetUsers", "Mission_Id", "Missions", "Id");
            DropColumn("AspNetUsers", "TicketList_Id");
            DropColumn("AspNetUsers", "Ticket_Id");
            DropColumn("Assets", "Ticket_Id");
            DropColumn("Ribbons", "Ticket_Id");
            DropTable("TicketLists");
            DropTable("Tickets");
        }
        
        public override void Down()
        {
            CreateTable(
                "Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        TicketList_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "TicketLists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("Ribbons", "Ticket_Id", c => c.Int());
            AddColumn("Assets", "Ticket_Id", c => c.Int());
            AddColumn("AspNetUsers", "Ticket_Id", c => c.Int());
            AddColumn("AspNetUsers", "TicketList_Id", c => c.Int());
            DropForeignKey("Missions", "MissionCommand_Id", "MissionCommands");
            DropForeignKey("AspNetUsers", "Mission_Id", "Missions");
            DropForeignKey("Ribbons", "Mission_Id", "Missions");
            DropForeignKey("Assets", "Mission_Id", "Missions");
            DropIndex("AspNetUsers", new[] { "Mission_Id" });
            DropIndex("Ribbons", new[] { "Mission_Id" });
            DropIndex("Assets", new[] { "Mission_Id" });
            DropIndex("Missions", new[] { "MissionCommand_Id" });
            DropColumn("Ribbons", "Mission_Id");
            DropColumn("Assets", "Mission_Id");
            DropColumn("AspNetUsers", "Mission_Id");
            DropTable("Missions");
            DropTable("MissionCommands");
            CreateIndex("Ribbons", "Ticket_Id");
            CreateIndex("Assets", "Ticket_Id");
            CreateIndex("Tickets", "TicketList_Id");
            CreateIndex("AspNetUsers", "Ticket_Id");
            CreateIndex("AspNetUsers", "TicketList_Id");
            AddForeignKey("Tickets", "TicketList_Id", "TicketLists", "Id");
            AddForeignKey("AspNetUsers", "Ticket_Id", "Tickets", "Id");
            AddForeignKey("Labels", "Ticket_Id", "Tickets", "Id");
            AddForeignKey("Attachments", "Ticket_Id", "Tickets", "Id");
            AddForeignKey("AspNetUsers", "TicketList_Id", "TicketLists", "Id");
            RenameTable(name: "Ribbons", newName: "Labels");
            RenameTable(name: "Assets", newName: "Attachments");
        }
    }
}
