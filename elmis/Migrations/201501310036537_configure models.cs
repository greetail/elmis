namespace elmis.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class configuremodels : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Lists",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        List_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Lists", t => t.List_Id)
                .Index(t => t.List_Id);
            
            CreateTable(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Location = c.String(),
                        Ticket_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tickets", t => t.Ticket_Id)
                .Index(t => t.Ticket_Id);
            
            CreateTable(
                "dbo.Labels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Ticket_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tickets", t => t.Ticket_Id)
                .Index(t => t.Ticket_Id);
            
            AddColumn("dbo.AspNetUsers", "List_Id", c => c.Int());
            AddColumn("dbo.AspNetUsers", "Ticket_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "List_Id");
            CreateIndex("dbo.AspNetUsers", "Ticket_Id");
            AddForeignKey("dbo.AspNetUsers", "List_Id", "dbo.Lists", "Id");
            AddForeignKey("dbo.AspNetUsers", "Ticket_Id", "dbo.Tickets", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "List_Id", "dbo.Lists");
            DropForeignKey("dbo.AspNetUsers", "Ticket_Id", "dbo.Tickets");
            DropForeignKey("dbo.Labels", "Ticket_Id", "dbo.Tickets");
            DropForeignKey("dbo.Attachments", "Ticket_Id", "dbo.Tickets");
            DropForeignKey("dbo.AspNetUsers", "List_Id", "dbo.Lists");
            DropIndex("dbo.Labels", new[] { "Ticket_Id" });
            DropIndex("dbo.Attachments", new[] { "Ticket_Id" });
            DropIndex("dbo.Tickets", new[] { "List_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "Ticket_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "List_Id" });
            DropColumn("dbo.AspNetUsers", "Ticket_Id");
            DropColumn("dbo.AspNetUsers", "List_Id");
            DropTable("dbo.Labels");
            DropTable("dbo.Attachments");
            DropTable("dbo.Tickets");
            DropTable("dbo.Lists");
        }
    }
}
