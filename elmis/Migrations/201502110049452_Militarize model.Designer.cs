// <auto-generated />
namespace elmis.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class Militarizemodel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Militarizemodel));
        
        string IMigrationMetadata.Id
        {
            get { return "201502110049452_Militarize model"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
