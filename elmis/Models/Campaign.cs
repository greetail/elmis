﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace elmis.Models {
	public class Campaign {
		public int Id { get; set; }
		public string Description { get; set; }
		public ICollection<MissionCommand> MissionCommands { get; set; }
		public string OwnerId { get; set; }
		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }
	}
}