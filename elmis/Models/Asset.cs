﻿using System;

namespace elmis.Models {
	public class Asset {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Location { get; set; }
		public string OwnerId { get; set; }
		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }
	}
}