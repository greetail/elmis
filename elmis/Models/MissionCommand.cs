﻿using System;
using System.Collections.Generic;

namespace elmis.Models {
	public class MissionCommand {
		public int Id { get; set; }
		public string Name { get; set; }
		public List<Mission> Missions { get; set; }
		public string OwnerId { get; set; }
		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }
	}
}