﻿using System;
using System.Drawing;

namespace elmis.Models {
	public class Ribbon {
		public int Id { get; set; }
		public string Name { get; set; }
		public Color Color { get; set; }
		public string OwnerId { get; set; }
		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }
	}
}