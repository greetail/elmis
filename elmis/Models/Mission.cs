﻿using System;
using System.Collections.Generic;

namespace elmis.Models {
	public class Mission {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public List<ApplicationUser> Soldiers { get; set; }
		public List<Ribbon> Ribbons { get; set; }
		public List<Asset> Assets { get; set; }
		public string OwnerId { get; set; }
		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }
	}
}