﻿function HomeViewModel(app, dataModel) {
	//data
	var self = this;
	self.error = ko.observable();
	//Campaign
	self.campaignDescription = ko.observable();
	self.campaigns = ko.observableArray([]);
	self.selectedCampaign = ko.observable();
	self.setSelectedCampaign = function(campaign) {
		self.selectedCampaign(campaign);
		//return true to allow default click action on buttons
		return true;
	}
	self.hideCreateCampaignButton = ko.observable();

	//missionCommands
	self.missionCommandName = ko.observable();
	self.missionCommands = ko.observableArray([]);
	self.selectedMissionCommand = ko.observable();
	self.setSelectedmissionCommand = function(missionCommand) {
		self.selectedMissionCommand(missionCommand);
		self.currentMissionCommandId(missionCommand.id());
	}
	self.hideCreateMissionCommand = ko.observable();
	self.currentCampaignId = ko.observable();

	//missions
	self.missionName = ko.observable();
	self.missions = ko.observableArray([]);
	self.selectedMission = ko.observable();
	self.setSelectedmission = function(mission) {
		self.selectedMission(mission);
		self.currentMissionId(mission.id());
	}
	self.hideCreateMission = ko.observable();
	self.currentMissionCommandId = ko.observable();
	self.currentMissionId = ko.observable();

	//Behaviours
	function ajaxHelper(uri, method, data) {
		self.error(''); // Clear error message
		return $.ajax({
			type: method,
			url: uri,
			dataType: 'json',
			contentType: 'application/json',
			headers: {
				'Authorization': 'Bearer ' + app.dataModel.getAccessToken()
			},
			data: data ? JSON.stringify(data) : null
		}).fail(function (jqXHR, textStatus, errorThrown) {
			if (errorThrown !== "") {
				self.error(errorThrown);
			} else { 
				self.error(textStatus);
			}
		});
	}
	//CRUD - campaign
	self.addCampaign = function () {
		var description = campaignDescription.value;
		var campaign = {
			Description: description,
			MissionCommands : []
		};
		self.campaignDescription("");
		ajaxHelper(app.dataModel.campaignsUrl, 'POST', campaign).done(function (data) {
			var savedCampaign = new campaignViewModel(data);
			self.campaigns.push(savedCampaign);
		});
	};

	self.deleteCampaign = function (campaign) {
		var uri = app.dataModel.campaignsUrl + campaign.id();
		ajaxHelper(uri, 'DELETE').done(function (data) {
			self.campaigns.remove(campaign);
			self.selectedCampaign(null);
		});
	};

	self.getCampaigns = function () {
		ajaxHelper(app.dataModel.campaignsUrl, 'GET').done(function (data) {
			var campaigns = $.map(data, function (item) { return new campaignViewModel(item); });
			self.campaigns(campaigns);
		});
	};

	self.updateCampaign = function (campaignToUpdate) {
		var uri = app.dataModel.campaignsUrl + campaignToUpdate.id();
		var campaign = {
			id: campaignToUpdate.id(),
			Description: campaignToUpdate.description(),
			ownerid: campaignToUpdate.ownerid(),
			created: campaignToUpdate.created(),
			MissionCommands: campaignToUpdate.missionCommands()
		};
		ajaxHelper(uri, 'PUT', campaign).done(function (data) {
			//todo: implement a jgrowl message https://github.com/stanlemon/jGrowl
		});
	};

	//Crud - missionscommands
	self.getMissionCommandsByCampaign = function (uri) {
		ajaxHelper(uri, 'GET').done(function (data) {
			var missionCommands = $.map(data, function (item) { return new missionCommandViewModel(item); });
			self.missionCommands(missionCommands);
		});
	};

	self.addMissionCommand = function() {
		var id = self.currentCampaignId();
		var uri = app.dataModel.campaignsUrl + id + "/missionCommands";
		var name = missionCommandName.value;
		var missionCommand = {
			Name: name
		};
		self.missionCommandName("");
		ajaxHelper(uri, 'POST', missionCommand).done(function (data) {
			var savedMissionCommand = new missionCommandViewModel(data);
			self.missionCommands.push(savedMissionCommand);
		});
	};

	self.deleteMissionCommand = function(missionCommand) {
		var id = self.currentCampaignId();
		var uri = app.dataModel.campaignsUrl + id + "/missionCommands/" + missionCommand.id();
		ajaxHelper(uri, 'DELETE').done(function (data) {
			self.missionCommands.remove(missionCommand);
			self.selectedMissionCommand(null);
		});
	};

	self.updateMissionCommand = function(missionCommandToUpdate) {
		var id = self.currentCampaignId();
		var uri = app.dataModel.campaignsUrl + id + "/missionCommands/" + missionCommandToUpdate.id();
		var missionCommandDto = {
			id: missionCommandToUpdate.id(),
			name: missionCommandToUpdate.name(),
			ownerid: missionCommandToUpdate.ownerid(),
			created: missionCommandToUpdate.created(),
			missions: missionCommandToUpdate.missions()
		};
		ajaxHelper(uri, 'PUT', missionCommandDto).done(function (data) {
			//todo: implement a jgrowl message https://github.com/stanlemon/jGrowl
		});
	};

	//Crud - missions
	self.addMission = function () {
		var campaignId = self.currentCampaignId();
		var missionCommandId = self.currentMissionCommandId();
		var uri = app.dataModel.campaignsUrl + campaignId + "/missionCommands/" + missionCommandId + "/missions";
		var name = missionName.value;
		var mission = {
			Name: name
		};
		self.missionName("");
		ajaxHelper(uri, 'POST', mission).done(function (data) {
			var savedMission = new missionViewModel(data);
			self.selectedMissionCommand().missions.push(savedMission);
		});
	};

	self.deleteMission = function (mission) {
		var campaignId = self.currentCampaignId();
		var missionCommandId = self.currentMissionCommandId();
		var uri = app.dataModel.campaignsUrl + campaignId + "/missionCommands/" + missionCommandId + "/missions/" + mission.id();
		ajaxHelper(uri, 'DELETE').done(function (data) {
			self.missions.remove(mission);
			self.selectedMission(null);
		});
	};

	self.updateMission = function (missionToUpdate) {
		var campaignId = self.currentCampaignId();
		var missionCommandId = self.currentMissionCommandId();
		var missionId = self.currentMissionId();
		var uri = app.dataModel.campaignsUrl + campaignId + "/missionCommands/" + missionCommandId + "/missions/" + missionId;
		var missionDto = {
			id: missionToUpdate.id(),
			name: missionToUpdate.name(),
			description: missionToUpdate.description(),
			ownerid: missionToUpdate.ownerid(),
			created: missionToUpdate.created()
		};
		ajaxHelper(uri, 'PUT', missionDto).done(function (data) {
			//todo: implement a jgrowl message https://github.com/stanlemon/jGrowl
		});
	};

	//Route functionality
	Sammy(function() {
		this.get('#campaigns', function () {
			//set everything else to null (hides the values)
			self.missionCommands(null);
			self.hideCreateCampaignButton(1);
			self.hideCreateMissionCommand(null);
			// Make a call to the protected Web API by passing in a Bearer Authorization Header
			self.getCampaigns();
		});

		this.get('#campaigns/:id/missionCommands', function () {
			self.currentCampaignId(this.params['id']);
			self.campaigns(null);
			self.hideCreateCampaignButton(null);
			self.hideCreateMissionCommand(1);
			var uri = app.dataModel.campaignsUrl + self.currentCampaignId() + "/missionCommands";
			self.getMissionCommandsByCampaign(uri);
		});

		//set the default route
		this.get('/', function () { this.app.runRoute('get', '#campaigns'); });
	});

	return self;
}

app.addViewModel({
	name: "Home",
	bindingMemberName: "home",
	factory: HomeViewModel
});