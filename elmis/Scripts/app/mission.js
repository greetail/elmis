﻿function missionViewModel(data) {
	var self = this;
	self.id = ko.observable(data.id);
	self.name = ko.observable(data.name);
	self.description = ko.observable(data.description);
	self.ownerid = ko.observable(data.ownerId);
	self.created = ko.observable(data.created);
	self.updated = ko.observable(data.updated);
}