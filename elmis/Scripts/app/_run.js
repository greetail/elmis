﻿$(
	function() {
		app.initialize();

		// Activate Knockout
		ko.validation.init({ grouping: { observable: false } });
		ko.applyBindings(app);

		//fix bogus facebook url
		if (window.location.href.indexOf("#_=_") > -1) {
			//remove facebook oAuth response bogus hash
			if (window.history && window.history.pushState) {
				history.pushState('', document.title, window.location.pathname);
			} else {
				window.location.href = window.location.href.replace(location.hash, "");
			}
		}
	}
);
