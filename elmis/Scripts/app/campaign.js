﻿function campaignViewModel(data) {
	var self = this;
	self.id = ko.observable(data.id);
	self.description = ko.observable(data.description);
	self.missionCommands = ko.observableArray(data.missionCommands);
	self.ownerid = ko.observable(data.ownerId);
	self.created = ko.observable(data.created);
	self.updated = ko.observable(data.updated);
	self.missionCommandsUrl = ko.computed(function() {
		return "#campaigns/" + self.id() +"/missionCommands";
	});
}