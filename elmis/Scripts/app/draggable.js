﻿$(function () {
	$(".ui-sortable*").sortable({
		connectWith: ".connectedSortable"
	}).disableSelection();
	$(".ui-draggable").draggable({
		connectToSortable: "ui-sortable",
		helper: "clone"
	});

	$(".ui-droppable").droppable(
		{
			accept: "ui-draggable",
		}
	);

});
