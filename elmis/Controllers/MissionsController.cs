﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using elmis.Models;
using Microsoft.AspNet.Identity;

namespace elmis.Controllers
{
    public class MissionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

		//GET: api/Campaigns/5/MissionCommands/1/missions
		[Route("api/Campaigns/{campaignId:int}/MissionCommands/{missionCommandId:int}/missions")]
		[ResponseType(typeof(List<Mission>))]
		public IHttpActionResult GetMissions(int campaignId, int missionCommandId) {
			//validate Id
			bool hasValidCampaignId = campaignId <= 0;
			bool hasValidMissionCommandId = missionCommandId <= 0;
			if (hasValidCampaignId) {
				return BadRequest("Invalid Campaign");
			}

			if (hasValidMissionCommandId) {
				return BadRequest("Invalid MissionCommand");
			}

			var campaign = db.Campaigns.Include(c => c.MissionCommands).FirstOrDefault(c => c.Id == campaignId);
			if (campaign == null || campaign.MissionCommands == null) {
				return NotFound();
			}

			var missions = campaign.MissionCommands.FirstOrDefault(mc => mc.Id == missionCommandId).Missions;

			return Ok(missions);
        }


        // PUT: api/campaigns/5/MissionCommands/1/missions/1
		[Route("api/Campaigns/{campaignId:int}/MissionCommands/{missionCommandId:int}/missions/{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMission(int id, Mission mission)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mission.Id)
            {
                return BadRequest();
            }

	        mission.Updated = DateTime.UtcNow;

            db.Entry(mission).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MissionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


		//POST: api/Campaigns/5/MissionCommands/31
	    [Route("api/Campaigns/{campaignId:int}/MissionCommands/{missionCommandId:int}/missions")]
	    [ResponseType(typeof (List<MissionCommand>))]
	    public IHttpActionResult PostMission(int campaignId, int missionCommandId, Mission mission) {
		    if (!ModelState.IsValid) {
			    return BadRequest(ModelState);
		    }

			string userId = User.Identity.GetUserId();
			mission.OwnerId = userId;
			mission.Created = DateTime.UtcNow;
			mission.Updated = DateTime.UtcNow;
		    db.Missions.Add(mission);

		    var missionCommand = db.Lists.Include(mc => mc.Missions).FirstOrDefault(mc => mc.Id == missionCommandId);

		    if (missionCommand == null) {
			    return NotFound();
		    }

		    if (!missionCommand.Missions.Any()) {
			    missionCommand.Missions = new List<Mission>();
		    }
			missionCommand.Missions.Add(mission);

		    db.SaveChanges();

			return Created(string.Format("api/Campaigns/{0}/MissionCommands/{1}/Missions/{2}", campaignId, missionCommandId, mission.Id), mission);
	    }
		
        // DELETE: api/Missions/5
        [ResponseType(typeof(Mission))]
        public async Task<IHttpActionResult> DeleteMission(int id)
        {
            Mission mission = await db.Missions.FindAsync(id);
            if (mission == null)
            {
                return NotFound();
            }

            db.Missions.Remove(mission);
            await db.SaveChangesAsync();

            return Ok(mission);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MissionExists(int id)
        {
            return db.Missions.Count(e => e.Id == id) > 0;
        }
    }
}