﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using elmis.Models;
using Microsoft.AspNet.Identity;

namespace elmis.Controllers
{
    public class CampaignsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Campaigns
		[ResponseType(typeof(IList<Campaign>))]
        public async Task<IHttpActionResult> GetCampaigns() {
			string userId = User.Identity.GetUserId();
			List<Campaign> campaigns = await db.Campaigns.Where(c => c.OwnerId == userId).ToListAsync();

	        return Ok(campaigns);
        }

        // PUT: api/Campaigns/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutCampaign(int id, Campaign campaign)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != campaign.Id)
            {
                return BadRequest();
            }

			//make sure only the owner or someone with access is able to update
			string userId = User.Identity.GetUserId();
	        if (!campaign.OwnerId.Equals(userId)) {
		        return NotFound();
	        }
			//set the update time
	        campaign.Updated = DateTime.UtcNow;

            db.Entry(campaign).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CampaignExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Campaigns
        [ResponseType(typeof(Campaign))]
        public async Task<IHttpActionResult> PostCampaign(Campaign campaign)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

	        campaign.OwnerId = User.Identity.GetUserId();
	        campaign.Created = DateTime.UtcNow;
	        campaign.Updated = DateTime.UtcNow;
			campaign.MissionCommands = new List<MissionCommand>();

            db.Campaigns.Add(campaign);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = campaign.Id }, campaign);
        }

        // DELETE: api/Campaigns/5
        [ResponseType(typeof(Campaign))]
        public IHttpActionResult DeleteCampaign(int id)
        {
            Campaign campaign = db.Campaigns.Include(c => c.MissionCommands).FirstOrDefault(c => c.Id == id);
            if (campaign == null)
            {
                return NotFound();
            }
			//make sure only the owner or someone with access is able to update
			string userId = User.Identity.GetUserId();
			if (!campaign.OwnerId.Equals(userId)) {
				return Unauthorized();
			}

			var missionCommands = campaign.MissionCommands.Select(missionCommand => db.Lists.Include(mc => mc.Missions).FirstOrDefault(mc => mc.Id == missionCommand.Id)).ToList();
	        foreach (var missionCommand in missionCommands) {
		        if (missionCommand.Missions.Any()) {
			        db.Missions.RemoveRange(missionCommand.Missions);
		        }
	        }

	        if (missionCommands.Any()) {
		        db.Lists.RemoveRange(missionCommands);
	        }

            db.Campaigns.Remove(campaign);
            db.SaveChanges();

            return Ok(campaign);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CampaignExists(int id)
        {
            return db.Campaigns.Count(e => e.Id == id) > 0;
        }
    }
}