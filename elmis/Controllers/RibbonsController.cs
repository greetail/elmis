﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using elmis.Models;

namespace elmis.Controllers
{
    public class RibbonsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Ribbons
        public IQueryable<Ribbon> GetRibbons()
        {
            return db.Ribbons;
        }

        // GET: api/Ribbons/5
        [ResponseType(typeof(Ribbon))]
        public async Task<IHttpActionResult> GetRibbon(int id)
        {
            Ribbon ribbon = await db.Ribbons.FindAsync(id);
            if (ribbon == null)
            {
                return NotFound();
            }

            return Ok(ribbon);
        }

        // PUT: api/Ribbons/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRibbon(int id, Ribbon ribbon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ribbon.Id)
            {
                return BadRequest();
            }

            db.Entry(ribbon).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RibbonExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Ribbons
        [ResponseType(typeof(Ribbon))]
        public async Task<IHttpActionResult> PostRibbon(Ribbon ribbon)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Ribbons.Add(ribbon);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = ribbon.Id }, ribbon);
        }

        // DELETE: api/Ribbons/5
        [ResponseType(typeof(Ribbon))]
        public async Task<IHttpActionResult> DeleteRibbon(int id)
        {
            Ribbon ribbon = await db.Ribbons.FindAsync(id);
            if (ribbon == null)
            {
                return NotFound();
            }

            db.Ribbons.Remove(ribbon);
            await db.SaveChangesAsync();

            return Ok(ribbon);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RibbonExists(int id)
        {
            return db.Ribbons.Count(e => e.Id == id) > 0;
        }
    }
}