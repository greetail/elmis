﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using elmis.ApiHelpers;
using elmis.Models;
using Microsoft.AspNet.Identity;

namespace elmis.Controllers
{
	[Authorize]
    public class MissionCommandsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

		//GET: api/Campaigns/5/MissionCommands
		[Route("api/Campaigns/{id:int}/MissionCommands")]
		[ResponseType(typeof(List<MissionCommand>))]
		public IHttpActionResult GetMissionCommandsByCampaign(int id) {
			if (id <= 0) {
				return BadRequest("Invalid campaign id");
			}

			var campaign = db.Campaigns.Include(c => c.MissionCommands).FirstOrDefault(c => c.Id == id);

			if (campaign == null) {
				return NotFound();
			}

			var missionCommands = campaign.MissionCommands.Select(command => db.Lists.Include(mc => mc.Missions).FirstOrDefault(mc => mc.Id == command.Id)).ToList();

			return Ok(missionCommands);
		}

		// PUT: api/Campaigns/5/MissionCommands/1
		[Route("api/Campaigns/{campaignId:int}/MissionCommands/{id:int}")]
        [ResponseType(typeof(void))]
		public async Task<IHttpActionResult> PutMissionCommand(int id, MissionCommand missionCommand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != missionCommand.Id)
            {
                return BadRequest();
            }

			missionCommand.Updated = DateTime.UtcNow;

            db.Entry(missionCommand).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketListExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

		// POST: api/Campaigns/5/MissionCommands
		[Route("api/Campaigns/{id:int}/MissionCommands")]
        [ResponseType(typeof(MissionCommand))]
		public async Task<IHttpActionResult> PostMissionCommandByCampaign(int id, MissionCommand missionCommand)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
			//set mission command values not in request
			string userId = User.Identity.GetUserId();
	        missionCommand.OwnerId = userId;
	        missionCommand.Created = DateTime.UtcNow;
	        missionCommand.Updated = DateTime.UtcNow;
			missionCommand.Missions = new List<Mission>();

			//get campaign by id
			Campaign campaign = db.Campaigns.Find(id);

			//save mission command to campaign
			if (campaign != null) {
				var missioncommands = new List<MissionCommand> {missionCommand};
				campaign.MissionCommands = missioncommands;
			}
			else {
				return NotFound();
			}
			//save to DB
            //db.Lists.Add(missionCommand);
            await db.SaveChangesAsync();

			return Created(string.Format("api/Campaigns/{0}/MissionCommands/{1}", campaign.Id, missionCommand.Id), missionCommand);
		}

		// DELETE: api/Campaigns/5/MissionCommands/1
		[Route("api/Campaigns/{campaignId:int}/MissionCommands/{id:int}")]
        [ResponseType(typeof(MissionCommand))]
		public IHttpActionResult DeleteMissionCommand(int campaignId, int id)
        {
			//string userId = User.Identity.GetUserId();
			//Campaign campaign = db.Campaigns.Find(id);
			MissionCommand missionCommand = db.Lists.Include(mc => mc.Missions).FirstOrDefault(mc => mc.Id == id);
            if (missionCommand == null)
            {
                return NotFound();
            }

			string userId = User.Identity.GetUserId();
			if (!missionCommand.OwnerId.Equals(userId)) {
				return Unauthorized();
			}

			if (missionCommand.Missions.Any()) {
				db.Missions.RemoveRange(missionCommand.Missions);
			}

            db.Lists.Remove(missionCommand);
            db.SaveChangesAsync();

            return Ok(missionCommand);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TicketListExists(int id)
        {
            return db.Lists.Count(e => e.Id == id) > 0;
        }
    }
}