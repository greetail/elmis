﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using elmis.ApiHelpers;

namespace elmis.Controllers {
	[Authorize]
	public class HomeController : Controller {
		public ActionResult Index() {
			ViewBag.Title = "Elmis.io";
			return View();
		}
	}
}
