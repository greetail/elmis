﻿using System.Web;
using System.Web.Mvc;

namespace elmis {
	public class FilterConfig {
		public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
			filters.Add(new HandleErrorAttribute());
			//have to use custom RequireHttpsAttribute in order for this to work in appharbor http://support.appharbor.com/kb/tips-and-tricks/ssl-and-certificates
			//filters.Add(new RequireHttpsAttribute());
			filters.Add(new AuthorizeAttribute());
		}
	}
}
