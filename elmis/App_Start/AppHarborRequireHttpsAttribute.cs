﻿using System;
using System.Net;
using System.Web.Mvc;
using RequireHttpsAttributeBase = System.Web.Mvc.RequireHttpsAttribute;

namespace elmis {
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true,
		AllowMultiple = false)]
	public class RequireHttpsAttribute : RequireHttpsAttributeBase {
		public override void OnAuthorization(AuthorizationContext filterContext) {
			if (filterContext == null) {
				throw new ArgumentNullException("filterContext");
			}

			if (filterContext.HttpContext.Request.IsSecureConnection) {
				return;
			}

			if (string.Equals(filterContext.HttpContext.Request.Headers["X-Forwarded-Proto"],
				"https",
				StringComparison.InvariantCultureIgnoreCase)) {
				return;
			}

			if (filterContext.HttpContext.Request.IsLocal) {
				return;
			}

			////non https
			//string url = "https://" + filterContext.HttpContext.Request.Url.Host + filterContext.HttpContext.Request.RawUrl;
			//filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.Forbidden, string.Format( "Must use https, try {0}", url));

			HandleNonHttpsRequest(filterContext);
		}
	}
}